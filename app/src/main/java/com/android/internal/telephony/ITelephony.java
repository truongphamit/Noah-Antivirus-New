package com.android.internal.telephony;

/**
 * Created by truon on 7/14/17.
 */

public interface ITelephony {
    boolean endCall();

    void answerRingingCall();

    void silenceRinger();
}
