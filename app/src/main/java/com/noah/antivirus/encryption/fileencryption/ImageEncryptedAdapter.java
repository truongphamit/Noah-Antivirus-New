package com.noah.antivirus.encryption.fileencryption;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.noah.antivirus.R;

import java.util.ArrayList;

/**
 * Created by Hungpq on 5/19/17.
 */

public class ImageEncryptedAdapter extends BaseAdapter {
    private ArrayList<ImageEncrypted> imagesEncrypted;
    private Context context;
    private LayoutInflater mInflater;

    public ImageEncryptedAdapter(ArrayList<ImageEncrypted> imagesEncrypted, Context context) {
        this.imagesEncrypted = imagesEncrypted;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return imagesEncrypted.size();
    }

    @Override
    public Object getItem(int i) {
        return imagesEncrypted.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ImageEncrypted imageEncrypted = imagesEncrypted.get(i);

        if (view == null) {
            view = mInflater.inflate(R.layout.grid_item_image, null);
        }

        final ImageView img = (ImageView) view.findViewById(R.id.item_img);
        final ImageView imgCheckbox = (ImageView) view.findViewById(R.id.item_check_box);


        Bitmap resized = ThumbnailUtils.extractThumbnail(imageEncrypted.getBitmap(), (int) Utils.convertDpToPixel(100, context), (int) Utils.convertDpToPixel(100, context));

        img.setImageBitmap(resized);

        if (imageEncrypted.isChecked()) {
            imgCheckbox.setBackgroundResource(R.drawable.checkbox_decrypt_bg);
            imgCheckbox.setImageResource(R.drawable.vector_check_box);
        } else {
            imgCheckbox.setBackgroundResource(R.drawable.checkbox_uncheck_decrypt_bg);
            imgCheckbox.setImageResource(android.R.color.transparent);
        }

        imgCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageEncrypted.isChecked()) imageEncrypted.setChecked(false);
                else imageEncrypted.setChecked(true);
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
