package com.noah.antivirus.encryption.fileencryption;

import android.graphics.Bitmap;

/**
 * Created by Hungpq on 5/19/17.
 */

public class ImageEncrypted {
    private Bitmap bitmap;
    private String filepath;
    private String fileName;
    private boolean isChecked;

    public ImageEncrypted(Bitmap bitmap, String fileName, String filepath, boolean isChecked) {
        this.bitmap = bitmap;
        this.fileName = fileName;
        this.filepath = filepath;
        this.isChecked = isChecked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
