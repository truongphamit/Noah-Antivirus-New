package com.noah.antivirus.activities;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.liulishuo.magicprogresswidget.MagicProgressBar;
import com.noah.antivirus.R;
import com.noah.antivirus.adapter.PagerAdapter;
import com.noah.antivirus.base.BaseNavigationDrawerActivity;
import com.noah.antivirus.model.AppData;
import com.noah.antivirus.util.ID;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.wifisecurity.provider.WifiSecuritySharePreference;

import butterknife.BindView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseNavigationDrawerActivity {

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent i = new Intent(this, MonitorShieldService.class);
        if (!Utils.isServiceRunning(this, MonitorShieldService.class)) {
            startService(i);
        }
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);

        if (getSharedPreferences("Settings", 0).getInt("rate", 0) == 1) {
            showRateDialog();
        }
    }

    boolean bound = false;
    private MonitorShieldService monitorShieldService;

    public MonitorShieldService getMonitorShieldService() {
        return monitorShieldService;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            monitorShieldService = null;
            bound = false;
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppEventsLogger.deactivateApp(this);
    }

    private void init() {
        viewpager.setAdapter(new PagerAdapter(getSupportFragmentManager(), this));
        tabs.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(2);
    }

    private void showRateDialog() {
        getSharedPreferences("Settings", 0).edit().putInt("rate", 2).apply();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_rate);
        dialog.show();

        ImageView btnClose = (ImageView) dialog.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ImageView btnRate = (ImageView) dialog.findViewById(R.id.btn_rate);
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }
}
