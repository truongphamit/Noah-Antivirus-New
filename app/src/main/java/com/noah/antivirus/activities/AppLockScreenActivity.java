package com.noah.antivirus.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.noah.antivirus.R;
import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.model.Selfie;
import com.noah.antivirus.service.LockService;
import com.noah.antivirus.util.Utils;
import com.takwolf.android.lock9.Lock9View;

import java.util.List;

import butterknife.BindView;

public class AppLockScreenActivity extends BaseToolbarActivity {
    private ImagesDatabaseHelper imagesDatabaseHelper;

    @BindView(R.id.lock_view)
    PatternLockView lock_view;

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.tv_forgot_password)
    TextView tv_forgot_password;

    private int countFailed;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_screen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);

        if (AppLockPreferencesManager.getVibrate(this)) {
            lock_view.setTactileFeedbackEnabled(true);
        } else {
            lock_view.setTactileFeedbackEnabled(false);
        }

        tv_forgot_password.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockScreenActivity.this, AppLockForgotPasswordActivity.class));
            }
        });

        lock_view.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                finish(PatternLockUtils.patternToString(lock_view, pattern));
            }

            @Override
            public void onCleared() {

            }
        });
    }

    private void finish(String password) {
        if (password.equals(AppLockPreferencesManager.getPassword(this))) {
            startActivity(new Intent(AppLockScreenActivity.this, AppLockHomeActivity.class));

            // Push notification
            if (AppLockPreferencesManager.getThieves(this)) {
                Image image = imagesDatabaseHelper.findByID(AppLockPreferencesManager.getAppThieves(this));
                if (image != null) {
                    Intent intent = new Intent(AppLockScreenActivity.this, AppLockImageActivity.class);
                    intent.putExtra("id", image.getId());
                    Utils.notificateAppLock(AppLockScreenActivity.this, LockService.NOTIFICATION_ID_APP_LOCK, R.mipmap.ic_thieves, "Someone tries to open your app.", image.getAppName(), "Someone tries to open your app.", intent);
                }
            }
            AppLockPreferencesManager.setThieves(this, false);

            finish();
        } else {
            lock_view.setViewMode(PatternLockView.PatternViewMode.WRONG);
            ++countFailed;
            if (countFailed == 3) {
                if (AppLockPreferencesManager.getSelfie(this)) {
                    (new Selfie(this, getPackageName())).takePhoto();
                }
            }
        }
    }
}
