package com.noah.antivirus.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.noah.antivirus.R;
import com.noah.antivirus.activities.AppLockForgotPasswordActivity;
import com.noah.antivirus.activities.AppLockImageActivity;
import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.model.Selfie;
import com.noah.antivirus.util.HomeWatcher;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.util.List;

public class LockService extends Service {
    public static final String ACTION_APPLICATION_PASSED = "com.star.applock.applicationpassedtest";
    public static final int NOTIFICATION_ID_APP_LOCK = 1111;

    private WindowManager windowManager;
    private View view;
    private ImageView img_app_icon;
    private TextView tv_app_name;
    private TextView tv_forgot_pass;
    private PatternLockView lock_view;

    private String pakageName;
    private int countFailed;
    private ImagesDatabaseHelper imagesDatabaseHelper;
    private HomeWatcher mHomeWatcher;

    public LockService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Bắt sự kiện Home và recents
        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                if (Utils.isServiceRunning(LockService.this, LockService.class)) {
                    stopSelf();
                }
            }

            @Override
            public void onHomeLongPressed() {
                if (Utils.isServiceRunning(LockService.this, LockService.class)) {
                    stopSelf();
                }
            }
        });
        mHomeWatcher.startWatch();

        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);
        windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams params;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }
        params.gravity = Gravity.TOP;
        params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.lock_view, null);

        ImageView img_close = (ImageView) view.findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(Utils.getHomeIntent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LockService.this.stopSelf();
                    }
                }, 500);
            }
        });

        img_app_icon = (ImageView) view.findViewById(R.id.img_app_icon);
        tv_app_name = (TextView) view.findViewById(R.id.tv_app_name);
        tv_forgot_pass = (TextView) view.findViewById(R.id.tv_forgot_password);
        tv_forgot_pass.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Utils.getHomeIntent());
                Intent intent = new Intent(LockService.this, AppLockForgotPasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                stopSelf();
            }
        });
        TypeFaceUttils.setNomal(this, tv_app_name);
        TypeFaceUttils.setNomal(this, tv_forgot_pass);

        lock_view = (PatternLockView) view.findViewById(R.id.lock_view);
        if (AppLockPreferencesManager.getVibrate(this)) {
            lock_view.setTactileFeedbackEnabled(true);
        } else {
            lock_view.setTactileFeedbackEnabled(false);
        }

        lock_view.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                finish(PatternLockUtils.patternToString(lock_view, pattern));
            }

            @Override
            public void onCleared() {

            }
        });

        windowManager.addView(view, params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pakageName = intent.getStringExtra("packageName");
        img_app_icon.setImageDrawable(Utils.getIconFromPackage(pakageName, this));
        tv_app_name.setText(Utils.getAppNameFromPackage(this, pakageName));
        return START_NOT_STICKY;
    }

    private void finish(String password) {
        if (password.equals(AppLockPreferencesManager.getPassword(this))) {
            sendBroadcast(new Intent().setAction(ACTION_APPLICATION_PASSED).putExtra("packageName", pakageName));

            // Push notification
            if (AppLockPreferencesManager.getThieves(this)) {
                Image image = imagesDatabaseHelper.findByID(AppLockPreferencesManager.getAppThieves(this));
                if (image != null) {
                    Intent intent = new Intent(this, AppLockImageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("id", image.getId());
                    Utils.notificateAppLock(this, NOTIFICATION_ID_APP_LOCK, R.mipmap.ic_thieves, getResources().getString(R.string.someone_tries_to_open_your_app), image.getAppName(), getResources().getString(R.string.someone_tries_to_open_your_app), intent);
                }
            }
            AppLockPreferencesManager.setThieves(this, false);

            if (AppLockPreferencesManager.getRelockPolicy(this)) {
                AppLockPreferencesManager.setUnlocked(this, true);
            }

            stopSelf();
        } else {
            lock_view.setViewMode(PatternLockView.PatternViewMode.WRONG);
            ++countFailed;
            if (countFailed == 3) {
                if (AppLockPreferencesManager.getSelfie(this)) {
                    (new Selfie(this, pakageName)).takePhoto();
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        mHomeWatcher.stopWatch();
        super.onDestroy();
        try {
            if (view != null) windowManager.removeView(view);
        } catch (Exception ignored) {

        }
    }
}
