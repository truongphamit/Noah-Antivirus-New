package com.noah.antivirus.activities;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.noah.antivirus.adapter.ApplicationsAdapter;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.dialogs.EnableAccessbilityDialog;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.service.BoosterService;
import com.noah.antivirus.util.AllowAccessibilityReceiverEvent;
import com.noah.antivirus.util.PreferencesManager;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import it.gmariotti.recyclerview.itemanimator.SlideInOutRightItemAnimator;

public class PhoneBoostActivity extends BaseToolbarActivity {
    public static String TAG = "PhoneBoostActivity";

    public static final String BROADCAST_ALLOW_ACCESSIBILITY = "BROADCAST_ALLOW_ACCESSIBILITY";
    public static final String SHOULD_BOOST = "SHOULD_BOOST";

    @BindView(R.id.rv_application)
    RecyclerView rv_application;

    @BindView(R.id.tv_count_running_app)
    TextView tv_count_running_app;

    @BindView(R.id.tv_memory_boost)
    TextView tv_memory_boost;

    @BindView(R.id.tv_boost)
    TextView tv_boost;

    @BindView(R.id.tv_mb)
    TextView tv_mb;

    @BindView(R.id.tv_freeable)
    TextView tv_freeable;

    @OnClick(R.id.tv_boost)
    public void onBoost(View view) {
        removeApps = new ArrayList<>();
        for (Application application : appsToClean) {
            if (application.isChoose()) {
                removeApps.add(application);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + this.getPackageName()));
                    startActivity(intent);
                    return;
                }
            }

            if (Utils.isAccessibilitySettingsOn(this)) {
                appsToClean.clear();
                Log.d(TAG, "btn boost pressed");
                boosterService.boost(removeApps, PhoneBoostActivity.this);

            } else {
                EnableAccessbilityDialog dialog = new EnableAccessbilityDialog(this);
                dialog.setCallBack(new EnableAccessbilityDialog.CallBack() {
                    @Override
                    public void execute() {
                        new RemoveApp().execute(removeApps);
                    }
                });
                dialog.show();
            }
        } else {
            new RemoveApp().execute(removeApps);
        }
    }

    private ApplicationsAdapter adapter;
    private ArrayList<Application> appsToClean;
    private List<Application> removeApps;

    private long totalBoost;

    boolean bound = false;
    private BoosterService boosterService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            BoosterService.BoosterBinder binder = (BoosterService.BoosterBinder) service;
            boosterService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            boosterService = null;
            bound = false;
        }
    };

    public int getLayoutId() {
        return R.layout.activity_phone_boost;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.phone_boost_menu);

        if (Utils.getCurrentTime() - PreferencesManager.getLastTimeBoost(this) >= 5 * 60 * 1000) {
            init();
            startService(new Intent(this, BoosterService.class));
            bindService(new Intent(this, BoosterService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            new GetRunningApp().execute();
        } else {
            Intent intent = new Intent(this, DoneBoostActivity.class);
            intent.putExtra(DoneBoostActivity.NO_RUNNING_APP, true);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if (bound && boosterService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    @Override
    public void onDestroy() {
        try {
            unregisterReceiver(allowAccessibilityService);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAllowAccessibilityServiceEvent(AllowAccessibilityReceiverEvent event) {
        if (event.isRegister()) {
            registerReceiver(allowAccessibilityService, new IntentFilter(BROADCAST_ALLOW_ACCESSIBILITY));
        }
    }

    /**
     * event when user allow accessibility service and auto boost device
     */
    public BroadcastReceiver allowAccessibilityService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boosterService.addViewInWindowManager();

                if (intent.getBooleanExtra(SHOULD_BOOST, false)) {
                    Log.d(TAG, "start auto boost");
                    boosterService.boost(removeApps, PhoneBoostActivity.this);
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };

    private void init() {
        rv_application.setLayoutManager(new LinearLayoutManager(this));
        rv_application.setHasFixedSize(true);
        rv_application.setItemAnimator(new SlideInOutRightItemAnimator(rv_application));
        appsToClean = new ArrayList<>();
        adapter = new ApplicationsAdapter(this, appsToClean);
        rv_application.setAdapter(adapter);
        adapter.setOnItemClickListener(new ApplicationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                CheckBox checkBox = (CheckBox) itemView;
                if (!checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    SharedPreferences sharedPreferences = getSharedPreferences(Utils.SHARE_PREFERENCE_APP_CHOSEN_ADVICE, Context.MODE_PRIVATE);
                    if (!sharedPreferences.contains(appsToClean.get(position).getPackageName())) {
                        sharedPreferences.edit().putInt(appsToClean.get(position).getPackageName(), 1).apply();

                    } else {
                        if (sharedPreferences.getInt(appsToClean.get(position).getPackageName(), 0) < 4) {
                            int timeUncheck = sharedPreferences.getInt(appsToClean.get(position).getPackageName(), 0);
                            timeUncheck++;
                            Log.d(TAG, "add app advice, " + appsToClean.get(position).getPackageName() + " " + timeUncheck);
                            sharedPreferences.edit().putInt(appsToClean.get(position).getPackageName(), timeUncheck).apply();
                        }
                    }

                    appsToClean.get(position).setChoose(false);
                    totalBoost -= appsToClean.get(position).getSize();
                } else {
                    checkBox.setChecked(true);
                    appsToClean.get(position).setChoose(true);
                    totalBoost += appsToClean.get(position).getSize();
                }
                updateBoostView();
            }
        });
    }

    private void updateBoostView() {
        if (totalBoost != 0) {
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            tv_boost.setVisibility(View.VISIBLE);
        } else {
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            tv_boost.setVisibility(View.GONE);
        }
    }

    private class GetRunningApp extends AsyncTask<Void, Application, Void> {
        private int totalSize;

        @Override
        protected Void doInBackground(Void... params) {
            ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            SharedPreferences sharedPreferences = null;
            try {
                sharedPreferences = getApplicationContext().getSharedPreferences(Utils.SHARE_PREFERENCE_APP_CHOSEN_ADVICE, Context.MODE_PRIVATE);
            } catch (Exception ignored) {
            }

            List<ActivityManager.RunningServiceInfo> runningServiceInfos = manager.getRunningServices(Integer.MAX_VALUE);
            for (ActivityManager.RunningServiceInfo serviceInfo : runningServiceInfos) {
                String packageName = serviceInfo.service.getPackageName();

                //Check System Process
                if (packageName.contains("android")) {
                    continue;
                }

                // Check process of this App
                if (packageName.equals(getApplicationContext().getPackageName())) {
                    continue;
                }

                Application application = new Application(serviceInfo.pid, packageName);
                application.setChoose(true);
                if (sharedPreferences != null) {
                    if (sharedPreferences.contains(packageName)) {
                        if (sharedPreferences.getInt(packageName, 0) >= 3) {
                            Log.d("get apps", "set app advice " + packageName);
                            application.setChoose(false);
                        }
                    }
                }

                if (packageName.contains("facebook")) {
                    application.setChoose(false);
                }

                if (packageName.contains("labankey")) {
                    application.setChoose(false);
                }

                application.setName(Utils.getAppNameFromPackage(getApplicationContext(), packageName));
                application.setIcon(Utils.getIconFromPackage(packageName, getApplicationContext()));
                application.setSize(Utils.getMemoryOfService(getApplicationContext(), serviceInfo.pid) / 1024);
                publishProgress(application);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Application... values) {
            super.onProgressUpdate(values);
            if (values[0] != null) {
                int index = appsToClean.indexOf(values[0]);
                if (index != -1) {
                    Application app = appsToClean.get(index);
                    app.setSize(values[0].getSize() + app.getSize());
                } else {
                    appsToClean.add(values[0]);
                }
                adapter.notifyDataSetChanged();
                totalSize += values[0].getSize();
                tv_memory_boost.setText(String.valueOf(totalSize));
                tv_count_running_app.setText(String.format(Locale.getDefault(), "%d %s", appsToClean.size(), getString(R.string.apps_running)));
                if (values[0].isChoose()) {
                    totalBoost += values[0].getSize();
                    updateBoostView();
                }
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (appsToClean.size() == 0) {
                Intent intent = new Intent(PhoneBoostActivity.this, DoneBoostActivity.class);
                intent.putExtra(DoneBoostActivity.NO_RUNNING_APP, true);
                startActivity(intent);
                PhoneBoostActivity.this.finish();
            }
        }
    }

    private class RemoveApp extends AsyncTask<List<Application>, Application, Void> {
        private int total;

        RemoveApp() {
            total = (int) totalBoost;
        }

        @Override
        protected Void doInBackground(List<Application>... params) {
            for (Application removeApp : params[0]) {
                Utils.killBackgroundProcesses(getApplicationContext(), removeApp.getPackageName());
                publishProgress(removeApp);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Application... values) {
            super.onProgressUpdate(values);
            adapter.notifyItemRemoved(appsToClean.indexOf(values[0]));
            appsToClean.remove(values[0]);
            total -= values[0].getSize();
            tv_memory_boost.setText(String.valueOf(total));
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            tv_count_running_app.setText(appsToClean.size() + " " + getResources().getString(R.string.apps_running));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            stopServiceBoost();
            PreferencesManager.setLastTimeBoost(PhoneBoostActivity.this, Utils.getCurrentTime());
            Intent intent = new Intent(PhoneBoostActivity.this, DoneBoostActivity.class);
            intent.putExtra(DoneBoostActivity.TOTAL_SIZE_CLEANED, totalBoost);
            startActivity(intent);
            PhoneBoostActivity.this.finish();
        }
    }

    public void stopServiceBoost() {
        stopService(new Intent(this, BoosterService.class));
    }

}
