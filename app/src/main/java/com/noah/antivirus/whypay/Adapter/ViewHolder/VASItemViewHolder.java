package com.noah.antivirus.whypay.Adapter.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Adapter.VASItemAdapter;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Event.SendSmsCancelEvent;
import com.noah.antivirus.whypay.Model.VASItem;
import com.noah.antivirus.whypay.Utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Hungpq on 4/11/17.
 */

public class VASItemViewHolder extends RecyclerView.ViewHolder {
    private TextView txtTitle, txtDescription, txtCancel;
    private Button btnCancelVas;

    public VASItemViewHolder(View itemView) {
        super(itemView);

        txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
        txtDescription = (TextView) itemView.findViewById(R.id.txt_description);
        txtCancel = (TextView) itemView.findViewById(R.id.txt_cancel);
        btnCancelVas = (Button) itemView.findViewById(R.id.btn_vas_cancel);
    }

    public void bind(final VASItem vasItem, final Activity activity, final int position, final VASItemAdapter adapter) {
        txtTitle.setText(vasItem.getTitle());
        if (vasItem.getDescription().equals("")) txtDescription.setVisibility(View.GONE);
        else {
            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(vasItem.getDescription());
        }

        txtCancel.setText(vasItem.getFormatCancel() + " gửi " + vasItem.getAddress());

        if (vasItem.isSendSmsCancel()) {
            Log.d("ViewHolder", "set null on click to position: " + getAdapterPosition());
            btnCancelVas.setEnabled(false);
        } else {
            btnCancelVas.setEnabled(true);
            btnCancelVas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("ViewHolder", "click to position: " + getAdapterPosition());
                    vasItem.setSendSmsCancel(true);

                    if (!vasItem.getAddress().equals("") && !vasItem.getFormatCancel().equals(""))
                        Utils.sendSms(activity, vasItem.getAddress(), vasItem.getFormatCancel(), false, false);
                    btnCancelVas.setEnabled(false);

                    EventBus.getDefault().post(new SendSmsCancelEvent(getAdapterPosition()));
                    adapter.notifyItemChanged(getAdapterPosition());
                }
            });
        }

        String addressPhoneTelecom = "";
        switch (activity.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).getInt(Constant.sharePreference_key_telecom_type, 99)) {
            case 0:
                addressPhoneTelecom = Constant.phone_telecom_vina;
                break;
            case 1:
                addressPhoneTelecom = Constant.phone_telecom_viettel;
                break;
            case 2:
                addressPhoneTelecom = Constant.phone_telecom_mobifone;
                break;
        }

        if (vasItem.getAddress().equals("") && vasItem.getFormatCancel().equals("")) {
            txtCancel.setText("Liên hệ tổng đài");
            btnCancelVas.setText("Gọi " + addressPhoneTelecom);
            btnCancelVas.setEnabled(false);
        }

    }
}
