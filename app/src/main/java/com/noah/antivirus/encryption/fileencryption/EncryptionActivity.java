package com.noah.antivirus.encryption.fileencryption;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


import com.noah.antivirus.R;
import com.noah.antivirus.encryption.fileencryption.Base.BaseToolbarActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import in.myinnos.awesomeimagepicker.activities.AlbumSelectActivity;
import in.myinnos.awesomeimagepicker.helpers.ConstantsCustomGallery;
import in.myinnos.awesomeimagepicker.models.Image;

public class EncryptionActivity extends BaseToolbarActivity {
    public static String TAG = "MainACtivity";
    private String environmentDirectory = Environment.getExternalStorageDirectory().toString();
    private static int RESULT_LOAD_IMG = 1;
    public static final int REQUEST_PERMISSION_CODE = 2;

    private ArrayList<ImageEncrypted> imageEncrypteds;
    private boolean isClickCheckAll = false;
    private ImageEncryptedAdapter adapter;


    @Override
    public int getLayoutId() {
        return R.layout.activity_encryption;
    }

    @Override
    public String getToolbarText() {
        return "Private Image";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Utils.checkPermission(EncryptionActivity.this)) {
            getAllEncryptFiles();
        } else Utils.requestPermission(EncryptionActivity.this);

        initDraw();
    }

    private void initDraw() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkTheSDCardPath(EncryptionActivity.this))
                    pickImage();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.encrypt_menu, menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE:
                if (Utils.checkPermission(EncryptionActivity.this)) {
                    getAllEncryptFiles();
                } else Utils.requestPermission(EncryptionActivity.this);

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (imageEncrypteds.size() != 0) {
            if (id == R.id.menu_check_all) {
                if (isClickCheckAll) {
                    isClickCheckAll = false;
                    for (ImageEncrypted imageEncrypted : imageEncrypteds) {
                        imageEncrypted.setChecked(false);
                    }
                    item.setIcon(getResources().getDrawable(R.drawable.ic_checkall));
                    adapter.notifyDataSetChanged();
                } else {
                    isClickCheckAll = true;
                    for (ImageEncrypted imageEncrypted : imageEncrypteds) {
                        imageEncrypted.setChecked(true);
                    }
                    item.setIcon(getResources().getDrawable(R.drawable.ic_checkall_ac));
                    adapter.notifyDataSetChanged();
                }

            } else if (id == R.id.menu_decrypt) {
                ArrayList<String> filePaths = new ArrayList<>();
                for (ImageEncrypted imageEncrypted : imageEncrypteds) {
                    if (imageEncrypted.isChecked()) filePaths.add(imageEncrypted.getFilepath());
                }

                new DecodeTask(getApplicationContext()) {
                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);

                        int size = imageEncrypteds.size();
                        for (int i = size - 1; i >= 0; i--) {
                            ImageEncrypted imageEncrypted = imageEncrypteds.get(i);
                            if (imageEncrypted.isChecked()) {
                                imageEncrypteds.remove(i);
                            }
                        }

                        adapter.notifyDataSetChanged();
                    }
                }.execute(filePaths);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void initDrawGridView() {

        GridView gridView = (GridView) findViewById(R.id.grid_view);
        adapter = new ImageEncryptedAdapter(imageEncrypteds, getApplicationContext());
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ImageEncrypted imageEncrypted = imageEncrypteds.get(i);
                saveImageInCache(imageEncrypted.getBitmap(), imageEncrypted.getFileName());
            }
        });
    }

    public void getAllEncryptFiles() {
        Log.d(TAG, Environment.getExternalStorageDirectory().toString());
        imageEncrypteds = new ArrayList<>();

        File dir = new File(environmentDirectory + "/" + Constant.FOLDER_ENCRYPT);
        final File names[] = dir.listFiles();

        if (names != null) {

            for (File name : names) {
                Log.d(TAG, "file encrypted: " + name.getAbsolutePath());

                decodeThumbnails(name);
            }

            Toast.makeText(getApplicationContext(), "Decode finished", Toast.LENGTH_SHORT).show();
            initDrawGridView();
        }


    }

    public void pickImage() {
        Intent intent = new Intent(this, AlbumSelectActivity.class);
        intent.putExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES, 10); // set limit for image selection
        startActivityForResult(intent, RESULT_LOAD_IMG);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                ArrayList<Image> images = data.getParcelableArrayListExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES);

                new EncodeTask(getApplicationContext()) {
                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);

                        getAllEncryptFiles();
                    }
                }.execute(images);

                Log.d(TAG, "File path: " + Arrays.toString(images.toArray()));

            } else if (requestCode == 4) {
                SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(Constant.sharePreference_key_setting, Context.MODE_PRIVATE).edit();

                if (resultCode == Activity.RESULT_OK) {
                    Uri treeUri = data.getData();
                    editor.putString(Constant.getSharePreference_uri_root_sdcard, treeUri.toString());
                    editor.apply();

                    // After confirmation, update stored value of folder.
                    // Persist access permissions.
                    int takeFlags = data.getFlags();
                    takeFlags &= (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        getContentResolver().takePersistableUriPermission(treeUri, takeFlags);
                    }

                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    /**
     * decoed file to show thumbnails
     */
    public void decodeThumbnails(File file) {

        String input = readFromFile(file);
        byte[] imageBytes = Base64.decode(input, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        Log.d(TAG, decodedByte.toString());

        imageEncrypteds.add(new ImageEncrypted(decodedByte, file.getName(), file.getAbsolutePath(), false));

//        img.setImageBitmap(decodedByte);
//        saveImageInCache(decodedByte);

    }


    public void saveImageInCache(Bitmap bitmap, String fileName) {
        String fileRealName = Utils.getFileRealName(fileName);
        Log.d(TAG, fileName + " " + fileRealName);
        fileName = fileRealName + "." + Constant.FILE_DECRYPT_EXTENSION;

        File file = new File(getExternalCacheDir(), fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);

        //API >= 24
//        intent.setDataAndType(Uri.fromFile(file), "image/*");
        Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);
        intent.setDataAndType(photoURI, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        Log.d(TAG, file.getAbsolutePath());

        startActivity(intent);

    }

    /**
     * read
     */
    public String readFromFile(File file) {
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            in.read(bytes);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        return new String(bytes);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCache(this);
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getExternalCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            if (dir.getAbsolutePath().contains(".jpg"))
                dir.delete();
        }
        return true;
    }

}
