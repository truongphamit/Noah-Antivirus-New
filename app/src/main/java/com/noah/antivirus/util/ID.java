package com.noah.antivirus.util;

/**
 * Created by truongpq on 23/03/2017.
 */

public class ID {
    // New
    public static final String INTERSTITIAL_AD = "ca-app-pub-3072585900661958/5369613827";
    public static final String NATIVE_AD_SCANED = "ca-app-pub-3072585900661958/8323080226";
    public static final String NATIVE_AD_FINISH = "ca-app-pub-3072585900661958/6846347028";
    public static final String BANNER_AD = "ca-app-pub-3072585900661958/3892880622";

    // Old
//    public static final String INTERSTITIAL_AD = "ca-app-pub-3072585900661958/5002855425";
//    public static final String NATIVE_AD_SCANED = "ca-app-pub-3072585900661958/9152544220";
//    public static final String NATIVE_AD_FINISH = "ca-app-pub-3072585900661958/8861826227";
//    public static final String BANNER_AD = "ca-app-pub-3072585900661958/3526122221";
}
