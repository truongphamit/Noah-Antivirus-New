package com.noah.antivirus.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.PhoneBoostActivity;
import com.noah.antivirus.adapter.ToolsAdapter;
import com.noah.antivirus.model.Tool;
import com.noah.antivirus.util.Constants;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.wifisecurity.WifiSecurityActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ToolsFragment extends Fragment {

    @BindView(R.id.rv_tools)
    RecyclerView rv_tools;

    public static ToolsFragment newInstance() {

        Bundle args = new Bundle();

        ToolsFragment fragment = new ToolsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ToolsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        rv_tools.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_tools.setHasFixedSize(true);
        List<Tool> tools = new ArrayList<>();
        tools.add(new Tool(Constants.TOOLS_WIFI, R.drawable.ic_banner_wifi, getString(R.string.tools_wifi_detection), R.drawable.tools_header_wifisecurity, getString(R.string.scan_wifi_now)));
        tools.add(new Tool(Constants.TOOLS_BROWSER, R.drawable.ic_banner_browser, getString(R.string.tools_safe_browser), R.drawable.tools_header_safebrowser, getString(R.string.use_now)));
        tools.add(new Tool(Constants.TOOLS_BOOST, R.drawable.ic_banner_boost, getString(R.string.tools_phone_boost), R.drawable.tools_header_phoneboost, getString(R.string.boost_now)));
        final ToolsAdapter adapter = new ToolsAdapter(getActivity(), tools);
        rv_tools.setAdapter(adapter);
        adapter.setOnItemClickListener(new ToolsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                switch (adapter.getTool(position).getId()) {
                    case Constants.TOOLS_BOOST:
                        startActivity(new Intent(getActivity(), PhoneBoostActivity.class));
                        break;
                    case Constants.TOOLS_WIFI:
                        startActivity(new Intent(getActivity(), WifiSecurityActivity.class));
                        break;
                }
            }
        });
    }

}
