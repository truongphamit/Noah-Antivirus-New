package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.Privacy;

import java.util.List;

/**
 * Created by truon on 8/14/17.
 */

public class PrivacyAdapter extends RecyclerView.Adapter<PrivacyAdapter.ViewHolder>{
    private Context context;
    private List<Privacy> privacies;

    public PrivacyAdapter(Context context, List<Privacy> privacies) {
        this.context = context;
        this.privacies = privacies;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_privacy, parent, false);

        // Return a new holder instance
        return new PrivacyAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Privacy privacy = privacies.get(position);

        holder.img_icon.setImageResource(privacy.getIcon());
        holder.tv_title.setText(privacy.getTitle());
    }

    @Override
    public int getItemCount() {
        return privacies.size();
    }

    public Privacy getPrivacy(int position) {
        return privacies.get(position);
    }

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img_icon;
        public TextView tv_title;

        public ViewHolder(final View itemView) {
            super(itemView);
            img_icon = (ImageView) itemView.findViewById(R.id.img_icon);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            // Setup the click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
