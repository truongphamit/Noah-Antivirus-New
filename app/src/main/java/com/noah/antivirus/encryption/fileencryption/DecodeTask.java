package com.noah.antivirus.encryption.fileencryption;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Hungpq on 5/19/17.
 * <p>
 * Decode image to jpg
 */

public class DecodeTask extends AsyncTask<ArrayList<String>, Integer, Boolean> {
    public static String TAG = "DecodeTask";
    private Context context;

    public DecodeTask(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(ArrayList<String>... files) {
        for (String filePath : files[0]) {
            File file = new File(filePath);
            decodeFile(file);
        }

        return true;
    }

    /**
     * decoed file which is encyrpt by base64 and imei hash
     */
    public void decodeFile(File file) {

        String input = readFromFile(file);
        byte[] imageBytes = Base64.decode(input, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        Log.d(TAG, decodedByte.toString());

        saveAsDecryptFile(decodedByte, file.getName());
        Utils.deleteFile(file, context);
    }

    public void saveAsDecryptFile(Bitmap bitmapImage, String fileName) {
//        String environmentDirectory = Environment.getExternalStorageDirectory().toString();
//        File myAppFolder = new File(environmentDirectory, Constant.FOLDER_DECRYPT);
//        myAppFolder.mkdir();

        File imageFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        String fileRealName = Utils.getFileRealName(fileName);
        Log.d(TAG, fileName + " " + fileRealName);

        fileName = fileRealName + "." + Constant.FILE_DECRYPT_EXTENSION;
        File mypath = new File(imageFolder, fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
                Utils.galleryAddPic(context, mypath.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * read
     *
     * @param file
     * @return
     */
    public String readFromFile(File file) {
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            in.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String contents = new String(bytes);
        return contents;
    }


}
