package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.noah.antivirus.R;
import com.noah.antivirus.fragment.PrivacyFragment;
import com.noah.antivirus.fragment.SecurityFragment;
import com.noah.antivirus.fragment.ToolsFragment;

/**
 * Created by truon on 8/10/17.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    private static final int PAGE_COUNT = 3;

    private Context context;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SecurityFragment.newInstance();
            case 1:
                return PrivacyFragment.newInstance();
            case 2:
                return ToolsFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getResources().getString(R.string.tab_title_security);
            case 1:
                return context.getResources().getString(R.string.tab_title_privacy);
            case 2:
                return context.getResources().getString(R.string.tab_title_tools);
            default:
                return null;
        }
    }
}
