package com.noah.antivirus.whypay.Constant;

/**
 * Created by Hungpq on 4/4/17.
 */

public class Constant {
    public static int telecom_type_vinaphone = 0;
    public static int telecom_type_viettel = 1;
    public static int telecom_type_mobifone = 2;
    public static int telecom_type_not_detect = 99;

    public static String sharePreference = "share_preference_key";
    public static String sharePreference_key_time_sent = "key_time_sent";

    public static String sharePreference_key_time_received = "key_time_received";

    public static String sharePreference_key_telecom_type = "key_telecommunication_type";

    public static String sharePreference_key_not_pick_tel_type = "key_tel_pick_sim_type";

    // processing when wait for sms to received
    public static String sharePreference_key_processing = "processing";

    public static String sharePreference_key_agree_send_sms = "agree_send_sms";

    public static int time_to_wait_sms = 2 * 60 * 1000;

    public static String address_vinaphone_check_vas = "123";
    public static String key_word_vinaphone_check_vas = "Tk";

    public static String address_viettel_check_vas = "1228";
    public static String key_word_viettel_check_vas = "TC";

    public static String address_mobifone_check_vas = "994";
    public static String key_word_mobifone_check_vas = "KT";

    public static String phone_telecom_mobifone = "9090";
    public static String phone_telecom_vina = "9191";
    public static String phone_telecom_viettel = "198";

}
