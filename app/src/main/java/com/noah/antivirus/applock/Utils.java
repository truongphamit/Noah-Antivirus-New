package com.noah.antivirus.applock;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.activities.AppLockScreenActivity;

/**
 * Created by truon on 8/25/17.
 */

public class Utils {
    public static void openAppLock(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!com.noah.antivirus.util.Utils.isUsageAccessEnabled(context)) {
                com.noah.antivirus.util.Utils.openUsageAccessSetings(context);
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(context)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
                context.startActivity(intent);
                return;
            }
        }

        if (!com.noah.antivirus.util.Utils.isAccessibilitySettingsOn(context)) {
            context.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));

            Dialog dialog = new Dialog(context);
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_guide_accessibility);
            dialog.show();
            return;
        }

        if (AppLockPreferencesManager.getPassword(context) != null) {
            context.startActivity(new Intent(context, AppLockScreenActivity.class));
        } else {
            context.startActivity(new Intent(context, AppLockCreatePasswordActivity.class));
        }
    }
}
