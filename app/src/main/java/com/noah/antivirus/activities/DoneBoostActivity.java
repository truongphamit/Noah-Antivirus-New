package com.noah.antivirus.activities;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.ID;
import com.noah.antivirus.util.Rotate3dAnimation;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.util.List;

import butterknife.BindView;

public class DoneBoostActivity extends BaseToolbarActivity {
    public static String sharePreference_key_boosting = "key_boosting_sharePreference";
    public static String sharePreference_key_rate_us = "key_rate_us";
    public static String sharePreference_key_times_show_popup_rate = "key_show_popup_rate";

    public static String TOTAL_SIZE_CLEANED = "TOTAL_SIZE_CLEANED";
    public static String NO_RUNNING_APP = "NO_RUNNING_APP";
    private ImageView imgRocket;
    private NativeExpressAdView adView;

    boolean shouldShowRateApp = false;

    @BindView(R.id.ads_container)
    FrameLayout ads_container;


    @Override
    public int getLayoutId() {
        return R.layout.activity_done_boost;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDraw();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

    }

    public void initDraw() {
        final SharedPreferences sharedPreferences = getSharedPreferences(sharePreference_key_boosting, MODE_PRIVATE);

        //Check if is no running app and not show ram released
        boolean isNoAppRunning = getIntent().getBooleanExtra(NO_RUNNING_APP, false);

        if (isNoAppRunning) {
            findViewById(R.id.layout_size_released).setVisibility(View.GONE);
            findViewById(R.id.layout_no_running_app).setVisibility(View.VISIBLE);
            imgRocket = (ImageView) findViewById(R.id.img_rocket_boosted);

        } else {
            long totalCleanedSize = getIntent().getLongExtra(TOTAL_SIZE_CLEANED, 0);
            ((TextView) findViewById(R.id.txt_size_total)).setText(Utils.roundFileSize(totalCleanedSize));
            ((TextView) findViewById(R.id.txt_unit_clean_total)).setText("MB");
            imgRocket = (ImageView) findViewById(R.id.img_rocket);

            if (totalCleanedSize > 0) {
                if (sharedPreferences.getInt(sharePreference_key_rate_us, 0) != 3) {

                    int countTime = sharedPreferences.getInt(sharePreference_key_times_show_popup_rate, 0);
                    if (countTime < 7) {
                        countTime++;
                        sharedPreferences.edit().putInt(sharePreference_key_times_show_popup_rate, countTime).commit();
                    }

                    if (sharedPreferences.getInt(sharePreference_key_rate_us, 0) == 0) {
                        sharedPreferences.edit().putInt(sharePreference_key_rate_us, 1).commit();
                    }

                    if (countTime == 3 || countTime == 6) {
                        sharedPreferences.edit().putInt(sharePreference_key_rate_us, 1).commit();
                    }
                }
            }

        }

        setupAds();


        (findViewById(R.id.btn_rate_app)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.rateUs(DoneBoostActivity.this);
                sharedPreferences.edit().putInt(sharePreference_key_rate_us, 3).commit();
            }
        });

        if (sharedPreferences.getInt(sharePreference_key_rate_us, 0) != 3) {
            shouldShowRateApp = true;
        }

        startRotation(-90, 0);

    }

    private void startRotation(float start, float end) {
        // Calculating center point
        final float centerX = Utils.convertDpToPixel(70, getApplicationContext()) / 2.0f;
        final float centerY = Utils.convertDpToPixel(70, getApplicationContext()) / 2.0f;
        // Create a new 3D rotation with the supplied parameter
        // The animation listener is used to trigger the next animation
        //final Rotate3dAnimation rotation =new Rotate3dAnimation(start, end, centerX, centerY, 310.0f, true);
        //Z axis is scaled to 0
        Rotate3dAnimation rotation = new Rotate3dAnimation(start, end, centerX, centerY, 0f, true);
        rotation.setDuration(1000);
        rotation.setFillAfter(true);
        //rotation.setInterpolator(new AccelerateInterpolator());
        //Uniform rotation
        rotation.setInterpolator(new LinearInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                adView.setVisibility(View.VISIBLE);
                if (shouldShowRateApp)
                    findViewById(R.id.layout_rate).setVisibility(View.VISIBLE);
                Animation showAdsAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.ads_show);
                findViewById(R.id.frame_ads_center).startAnimation(showAdsAnimation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        //Monitor settings
        imgRocket.startAnimation(rotation);
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


    private void setupAds() {
        adView = new NativeExpressAdView(this);
        adView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 320));
        adView.setAdUnitId(ID.NATIVE_AD_FINISH);
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        ads_container.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }
}

