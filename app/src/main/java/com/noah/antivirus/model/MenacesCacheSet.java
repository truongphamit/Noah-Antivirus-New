package com.noah.antivirus.model;

import android.content.Context;

import com.noah.antivirus.iface.IProblem;


/**
 * Created by hexdump on 22/01/16.
 */
public class MenacesCacheSet extends JSONDataSet<IProblem>
{
    public MenacesCacheSet(Context context)
    {
        super(context,"menacescache.json",new ProblemFactory());
    }
}
