package com.noah.antivirus.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.widget.TextView;

import com.blankj.utilcode.util.DeviceUtils;
import com.liulishuo.magicprogresswidget.MagicProgressBar;
import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.Utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class PhoneInfoActivity extends BaseToolbarActivity {

    @BindView(R.id.pb_cpu)
    MagicProgressBar pb_cpu;

    @BindView(R.id.pb_ram)
    MagicProgressBar pb_ram;

    @BindView(R.id.pb_storage)
    MagicProgressBar pb_storage;

    @BindView(R.id.tv_basic_information)
    TextView tv_basic_information;

    @BindView(R.id.tv_imei)
    TextView tv_imei;

    @BindView(R.id.tv_system_os_version)
    TextView tvSystOSVersion;

    @BindView(R.id.tv_root_state)
    TextView tvRootState;

    @BindView(R.id.tv_info_storage)
    TextView tvInforStorage;

    @BindView(R.id.tv_info_ram)
    TextView tvInforRam;

    @BindView(R.id.tv_info_cpu)
    TextView tvInfoCPU;

    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    public int getLayoutId() {
        return R.layout.activity_phone_info;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getResources().getString(R.string.phone_info));

        init();
        startTimer();
    }

    private void init() {
        tvSystOSVersion.setText(String.format("Android %s", String.valueOf(Build.VERSION.RELEASE)));
        if (DeviceUtils.isDeviceRooted()) {
            tvRootState.setText(R.string.rooted);
        } else {
            tvRootState.setText(R.string.not_rooted);
        }

        if (Build.VERSION.SDK_INT < 23) {
            tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
        } else {
            boolean permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
            if (!permission) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            } else {
                tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
            }
        }

        long totalInternalMemorySize = Utils.getTotalInternalMemorySize();
        long availableInternalMemorySize = Utils.getAvailableInternalMemorySize();
        tvInforStorage.setText(Utils.formatSize(totalInternalMemorySize - availableInternalMemorySize));
        pb_storage.setSmoothPercent((float) (totalInternalMemorySize - Utils.getAvailableInternalMemorySize()) / totalInternalMemorySize);

        long freeRam = Utils.getFreeRAM(this);
        long totalRam = Utils.getTotalRAM(this);
        tvInforRam.setText(Utils.formatSize(totalRam - freeRam));
        pb_ram.setSmoothPercent(((float) (totalRam - freeRam) / totalRam));
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 500, 1000); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        int cpu = (int) (readUsage() * 100);
                        tvInfoCPU.setText(cpu + "%");
                        pb_cpu.setSmoothPercent((float) (cpu / 100.0));
                    }
                });
            }
        };
    }


    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception e) {
            }

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float) (cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                stoptimertask();
                finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stoptimertask();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stoptimertask();
    }
}
