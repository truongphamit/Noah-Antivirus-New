package com.noah.antivirus.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

/**
 * Created by truon on 8/15/17.
 */

public class PreferencesManager {
    // Boost
    private static final String PREFERENCES_LAST_TIME_BOOST = "LAST_TIME_BOOST";

    public static SharedPreferences getPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    // Boost
    public static void setLastTimeBoost(Context context, long time) {
        getPreference(context).edit().putLong(PREFERENCES_LAST_TIME_BOOST, Utils.getCurrentTime()).apply();
    }

    public static long getLastTimeBoost(Context context) {
        return getPreference(context).getLong(PREFERENCES_LAST_TIME_BOOST, 0);
    }
}
