package com.noah.antivirus.activities;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.ID;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;

import butterknife.BindView;

public class SafeActivity extends BaseToolbarActivity {

    @BindView(R.id.ll_done)
    View ll_done;

    @BindView(R.id.layout_bottom)
    View layout_bottom;

    @BindView(R.id.layout_falcon)
    View layout_falcon;

    @BindView(R.id.img_rocket)
    ImageView img_rocket;

    @BindView(R.id.tv_falcon_name)
    TextView tv_falcon_name;

    @BindView(R.id.tv_falcon_descriptin)
    TextView tv_falcon_descriptin;

    @BindView(R.id.tv_free_download)
    TextView tv_free_download;

    @BindView(R.id.ads_container)
    FrameLayout ads_container;

    @BindView(R.id.txt_title1)
    TextView txt_title1;

    @BindView(R.id.tv_title)
    TextView tv_title;

    private NativeExpressAdView mNativeExpressAdView;

    private boolean shouldShowRateApp = false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_safe;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        String title = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        if (title != null) {
            txt_title1.setText(title);
            tv_title.setText(title);
        }

        if (Utils.isPackageInstalled(this, "com.noah.falconcleaner")) {
            layout_falcon.setVisibility(View.GONE);
        } else {
            TypeFaceUttils.setNomal(this, tv_falcon_name);
            TypeFaceUttils.setNomal(this, tv_falcon_descriptin);
            TypeFaceUttils.setNomal(this, tv_free_download);

            tv_free_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.noah.falconcleaner")));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.noah.falconcleaner")));
                    }
                }
            });
        }

        final SharedPreferences sharedPreferences = getSharedPreferences(DoneBoostActivity.sharePreference_key_boosting, MODE_PRIVATE);
        (findViewById(R.id.btn_rate_app)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.rateUs(SafeActivity.this);
                sharedPreferences.edit().putInt(DoneBoostActivity.sharePreference_key_rate_us, 3).apply();
            }
        });

        if (sharedPreferences.getInt(DoneBoostActivity.sharePreference_key_rate_us, 0) != 3) {
            shouldShowRateApp = true;
        }

        final AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.flip_left_in);
        set.setTarget(img_rocket);

        final Animation animShow = AnimationUtils.loadAnimation(SafeActivity.this, R.anim.view_show);
        final Animation animation = AnimationUtils.loadAnimation(SafeActivity.this, R.anim.view_show);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (shouldShowRateApp)
                    findViewById(R.id.layout_rate).setVisibility(View.VISIBLE);

                layout_bottom.setVisibility(View.VISIBLE);
                layout_bottom.startAnimation(animShow);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        setupAds();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                ll_done.startAnimation(animation);
                set.start();
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Resume the NativeExpressAdView.
        mNativeExpressAdView.resume();
    }

    @Override
    protected void onPause() {
        // Pause the NativeExpressAdView.
        mNativeExpressAdView.pause();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mNativeExpressAdView.destroy();

        super.onDestroy();
    }

    private void setupAds() {
        mNativeExpressAdView = new NativeExpressAdView(this);
        mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 320));
        mNativeExpressAdView.setAdUnitId(ID.NATIVE_AD_FINISH);
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        ads_container.addView(mNativeExpressAdView);
        mNativeExpressAdView.loadAd(adRequestBuilder.build());
    }
}
