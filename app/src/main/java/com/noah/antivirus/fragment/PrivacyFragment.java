package com.noah.antivirus.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.PrivacyAdapter;
import com.noah.antivirus.applock.Utils;
import com.noah.antivirus.base.BaseNavigationDrawerActivity;
import com.noah.antivirus.encryption.fileencryption.EncryptionActivity;
import com.noah.antivirus.model.Privacy;
import com.noah.antivirus.util.Constants;
import com.noah.antivirus.whypay.Activity.VasCheckerActivity;
import com.noah.antivirus.whypay.Constant.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyFragment extends Fragment {

    @BindView(R.id.rv_privacy)
    RecyclerView rv_privacy;

    public static PrivacyFragment newInstance() {

        Bundle args = new Bundle();

        PrivacyFragment fragment = new PrivacyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public PrivacyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_privacy, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        rv_privacy.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_privacy.setHasFixedSize(true);
        List<Privacy> privacies = new ArrayList<>();
        privacies.add(new Privacy(Constants.PRIVACY_APPLOCK, getString(R.string.privacy_apps_lock), R.drawable.bt_apps_lock));
        privacies.add(new Privacy(Constants.PRIVACY_NOTE, getString(R.string.privacy_private_note), R.drawable.bt_private_note));
        privacies.add(new Privacy(Constants.PRIVACY_IMAGE, getString(R.string.privacy_private_image), R.drawable.bt_private_image));
        privacies.add(new Privacy(Constants.PRIVACY_VIDEO, getString(R.string.provacy_private_video), R.drawable.bt_private_video));
        privacies.add(new Privacy(Constants.PRIVACY_CALL, getString(R.string.privacy_private_call), R.drawable.bt_private_call));
        privacies.add(new Privacy(Constants.PRIVACY_ALERT, getString(R.string.privacy_breakin_alert), R.drawable.bt_private_break_in_alert));
        privacies.add(new Privacy(Constants.PRIVACY_VAS, getString(R.string.privacy_vas), R.drawable.bt_private_vas));
        final PrivacyAdapter adapter = new PrivacyAdapter(getActivity(), privacies);
        rv_privacy.setAdapter(adapter);
        adapter.setOnItemClickListener(new PrivacyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                switch (adapter.getPrivacy(position).getId()) {
                    case Constants.PRIVACY_APPLOCK:
                        Utils.openAppLock(getActivity());
                        break;
                    case Constants.PRIVACY_VAS:
                        startActivity(new Intent(getContext(), VasCheckerActivity.class));
                        break;
                    case Constants.PRIVACY_IMAGE:
                        startActivity(new Intent(getContext(), EncryptionActivity.class));
                        break;
                }
            }
        });
    }
}
