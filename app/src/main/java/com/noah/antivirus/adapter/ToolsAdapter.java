package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.Privacy;
import com.noah.antivirus.model.Tool;

import java.util.List;

/**
 * Created by truon on 8/14/17.
 */

public class ToolsAdapter extends RecyclerView.Adapter<ToolsAdapter.ViewHolder> {
    private Context context;
    private List<Tool> tools;

    public ToolsAdapter(Context context, List<Tool> tools) {
        this.context = context;
        this.tools = tools;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_tools, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Tool tool = tools.get(position);

        holder.img_icon.setImageResource(tool.getResIcon());
        holder.img_header.setImageResource(tool.getResHeader());
        holder.tv_title.setText(tool.getTitle());
        holder.tv_action.setText(tool.getAction());
    }

    @Override
    public int getItemCount() {
        return tools.size();
    }

    public Tool getTool(int position) {
        return tools.get(position);
    }

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img_icon;
        public TextView tv_title;
        public ImageView img_header;
        public TextView tv_action;

        public ViewHolder(final View itemView) {
            super(itemView);
            img_icon = (ImageView) itemView.findViewById(R.id.img_icon);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            img_header = (ImageView) itemView.findViewById(R.id.img_header);
            tv_action = (TextView) itemView.findViewById(R.id.tv_action);
            // Setup the click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
