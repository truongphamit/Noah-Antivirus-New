package com.noah.antivirus.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.noah.antivirus.R;
import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.takwolf.android.lock9.Lock9View;

import java.util.List;

import butterknife.BindView;

public class AppLockEditPasswordActivity extends BaseToolbarActivity {

    @BindView(R.id.lock_view)
    PatternLockView lock_view;

    @BindView(R.id.tv_title)
    TextView tv_title;

    private String password;
    private int step;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_edit_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        if (AppLockPreferencesManager.getVibrate(this)) {
            lock_view.setTactileFeedbackEnabled(true);
        } else {
            lock_view.setTactileFeedbackEnabled(false);
        }

        lock_view.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                switch (step) {
                    case 0:
                        if (PatternLockUtils.patternToString(lock_view, pattern).equals(AppLockPreferencesManager.getPassword(AppLockEditPasswordActivity.this))) {
                            lock_view.setViewMode(PatternLockView.PatternViewMode.CORRECT);
                            lock_view.clearPattern();
                            tv_title.setText(R.string.set_a_new_password);
                            ++step;
                        } else {
                            lock_view.setViewMode(PatternLockView.PatternViewMode.WRONG);
                        }
                        break;
                    case 1:
                        password = PatternLockUtils.patternToString(lock_view, pattern);
                        lock_view.clearPattern();
                        tv_title.setText(R.string.enter_password_again);
                        ++step;
                        break;
                    case 2:
                        if (!password.equals(PatternLockUtils.patternToString(lock_view, pattern))) {
                            lock_view.setViewMode(PatternLockView.PatternViewMode.WRONG);
                        } else {
                            AppLockPreferencesManager.setPassword(AppLockEditPasswordActivity.this, password);
                            finish();
                        }
                        break;
                }
            }

            @Override
            public void onCleared() {

            }
        });
    }
}
