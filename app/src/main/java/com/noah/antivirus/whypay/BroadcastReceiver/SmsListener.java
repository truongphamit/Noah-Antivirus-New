package com.noah.antivirus.whypay.BroadcastReceiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.noah.antivirus.whypay.Activity.VASResultActivity;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Event.ConfirmSmsCancelEvent;
import com.noah.antivirus.whypay.Model.ResultVas;
import com.noah.antivirus.whypay.Model.VASItem;
import com.noah.antivirus.whypay.Notification.ResultNotification;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hungpq on 4/4/17.
 */

public class SmsListener extends BroadcastReceiver {
    public String TAG = "SmsListener";
    private SharedPreferences sharedPreferences;
    private ResultVas resultVas;

    private ArrayList<VASItem> vasItems;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "received intent sms received");
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from = "";
            String msgBody = "";
            if (bundle != null) {
                //---retrieve the SMS message received---
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();
                        msgBody += msgs[i].getMessageBody();

                    }
//                    Utils.deleteSms(context);

                    if (msg_from.equals(Constant.address_viettel_check_vas) | msg_from.equals(Constant.address_vinaphone_check_vas) | msg_from.equals(Constant.address_mobifone_check_vas)) {
                        vasItems = new ArrayList<>();
                        analyzeMessage(context, msg_from, msgBody);
                    } else {
                        EventBus.getDefault().post(new ConfirmSmsCancelEvent(msg_from, msgBody));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void analyzeMessage(Context context, String addressFrom, String msgBody) {
        Log.d(TAG, addressFrom + " " + msgBody);

        sharedPreferences = context.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE);
        long timeSent = sharedPreferences.getLong(Constant.sharePreference_key_time_sent, 0);
        long timeReceived = sharedPreferences.getLong(Constant.sharePreference_key_time_received, 0);

        /**
         * only if time sent is smaller then time received then analyze else the message is analyzed
         */
        if (timeSent > timeReceived) {
            int typeTel = sharedPreferences.getInt(Constant.sharePreference_key_telecom_type, 0);

            if (addressFrom.equals(Constant.address_vinaphone_check_vas) && typeTel == Constant.telecom_type_vinaphone)
                analyzeVinaPhoneVas(context, msgBody);
            else if (addressFrom.equals(Constant.address_viettel_check_vas) && typeTel == Constant.telecom_type_viettel)
                analyzeViettelVas(context, msgBody);
            else if (addressFrom.equals(Constant.address_mobifone_check_vas) && typeTel == Constant.telecom_type_mobifone)
                analyzeMobifoneVas(context, msgBody);
        }

    }

    public void analyzeVinaPhoneVas(Context context, String msgBody) {

        if (msgBody.contains("khong dang ky su dung dich vu nao")) {
//            Toast.makeText(context, "Hien khong co dich vu nao", Toast.LENGTH_LONG).show();
            showResult(context);
        } else {

            String vas = "";
            Pattern pattern = Pattern.compile(".+dich\\svu:(.+)\\.\\sDe\\sbiet\\sthem.+");
            Matcher matcher = pattern.matcher(msgBody);

            if (matcher.find()) {
                Log.d(TAG, "matcher found");
                vas = matcher.group(1);
            }

            Log.d(TAG, "vas: " + vas);
            String[] vass = vas.split(",");

            for (String itemVas : vass) {
                itemVas = itemVas.trim();

                Log.d(TAG, itemVas);

                Pattern pattern1 = Pattern.compile("\\s*(.+)\\s*\\((.+):(\\d+)\\)");
                Matcher matcher1 = pattern1.matcher(itemVas);

                if (matcher1.find()) {
                    String title = matcher1.group(1);
                    String formatCancel = matcher1.group(2);
                    String address = matcher1.group(3);
                    VASItem vasItem = new VASItem(title, "", address, formatCancel);
                    vasItems.add(vasItem);

                    Log.d(TAG, vasItem.toString());
                }
            }

            showResult(context);
        }

        updateTime();
    }

    public void analyzeViettelVas(Context context, String msgBody) {
        if (msgBody.contains("khong su dung dich vu") || msgBody.contains("chua dang ky dich vu")) {
//            Toast.makeText(context, "Hien khong co dich vu nao", Toast.LENGTH_LONG).show();
            showResult(context);
        } else {
            if (msgBody.contains(". Chi tiet"))
                analyzeViettelVas1(context, msgBody);
            else analyzeViettelVas2(context, msgBody);
        }

        updateTime();
    }

    public void analyzeViettelVas1(Context context, String msgBody) {
        String vas = "";
        String vasCancel = "";

        String keywordBeginVas = "DV: ";
        int begin = msgBody.indexOf(keywordBeginVas);
        int end = msgBody.indexOf(". De");

        vas = msgBody.substring(begin + keywordBeginVas.length(), end);
        int endVasCancel = msgBody.indexOf(". Chi tiet");
        if (endVasCancel > 0) vasCancel = msgBody.substring(end, endVasCancel);

        String[] vass = vas.split(",");
        for (String itemVas : vass) {
            vasItems.add(new VASItem(itemVas, "", "", ""));
        }

        String[] vasCancels = vasCancel.trim().split(". De huy");
        int pos = 0;
        for (String vasCancel1 : vasCancels) {
            String itemVasCancel = vasCancel1.trim();

            Pattern pattern = Pattern.compile("(.+)\\ssoan\\s(.+)\\sgui\\s(\\d+)");
            Matcher matcher = pattern.matcher(itemVasCancel);

            if (!itemVasCancel.equals("") && matcher.find()) {
                String formatCancel = matcher.group(2).trim();
                String address = matcher.group(3).trim();

                vasItems.get(pos).setAddress(address);
                vasItems.get(pos).setFormatCancel(formatCancel);

                Log.d(TAG, "add vas item: " + " " + vasItems.get(pos).toString());
                pos++;
            }
        }

        showResult(context);
    }

    public void analyzeViettelVas2(Context context, String msgBody) {
        String keywordBeginVas = "DV:";
        int begin = msgBody.indexOf(keywordBeginVas);

        Log.d(TAG, "begin: " + begin);
        String vas = msgBody.substring(begin + keywordBeginVas.length());

        Log.d(TAG, "vas: " + vas);
        String[] vass = vas.split("\n");

        for (String itemVas : vass) {
            Log.d(TAG, itemVas.trim());

            // pattern if vas has many service it has number in the first
            Pattern pattern = Pattern.compile("(\\d+)\\.(.+)(\\(\\d+.+\\/.+\\))\\.(.+)\\.");
            Matcher matcher = pattern.matcher(itemVas);

            if (matcher.find()) {
                String content = matcher.group(2).trim();
                String desc = matcher.group(3).trim();
                VASItem vasItem = new VASItem(content, desc, "", "");

                String stringCancel = matcher.group(4).trim();
                Pattern pattern1 = Pattern.compile("De\\shuy,\\ssoan\\s(.+);.+\\sgui\\s(\\d+)");
                Matcher matcher1 = pattern1.matcher(stringCancel);

                if (matcher1.find()) {
                    String formatCancel = matcher1.group(1).trim();
                    String address = matcher1.group(2).trim();

                    vasItem.setAddress(address);
                    vasItem.setFormatCancel(formatCancel);
                    vasItems.add(vasItem);

                    Log.d(TAG, "add vas item: " + vasItem.toString());
                }

            } else {

                // only 1 service vas found
                pattern = Pattern.compile("(.+)(\\(\\d+.+\\/.+\\))\\.(.+)\\.");
                matcher = pattern.matcher(itemVas);

                if (matcher.find()) {
                    String content = matcher.group(1).trim();
                    String desc = matcher.group(2).trim();
                    VASItem vasItem = new VASItem(content, desc, "", "");

                    String stringCancel = matcher.group(3).trim();
                    Pattern pattern1 = Pattern.compile("De\\shuy,\\ssoan\\s(.+);.+\\sgui\\s(\\d+)");
                    Matcher matcher1 = pattern1.matcher(stringCancel);

                    if (matcher1.find()) {
                        String formatCancel = matcher1.group(1).trim();
                        String address = matcher1.group(2).trim();

                        vasItem.setAddress(address);
                        vasItem.setFormatCancel(formatCancel);
                        vasItems.add(vasItem);

                        Log.d(TAG, "add vas item: " + vasItem.toString());
                    }
                }
            }
        }

        showResult(context);
    }

    public void analyzeMobifoneVas(Context context, String msgBody) {
        if (msgBody.contains("khong") && msgBody.contains("dich vu")) {
            showResult(context);
        } else {

            String keyword = "";
            Pattern pattern = Pattern.compile(".+dich\\svu\\ssau:(.+)\\.\\sDe\\sbiet\\sthem.+");
            Matcher matcher = pattern.matcher(msgBody);

            if (matcher.find()) {
                keyword = matcher.group(1);
                Log.d(TAG, "matcher found: " + keyword);
            }

            pattern = Pattern.compile("(.+)\\.\\s*De\\shuy\\sdich\\svu:(.+)");
            matcher = pattern.matcher(keyword);

            if (matcher.find()) {
                String textVas = matcher.group(1).trim();
                String textCancel = matcher.group(2).trim();


                String[] textVass = textVas.split("\\),");
                for (String textVas1 : textVass) {
                    String itemTextVas = textVas1.trim();
                    if (!itemTextVas.equals("")) {

                        pattern = Pattern.compile("(.+)\\((.+)\\)*");
                        matcher = pattern.matcher(itemTextVas);
                        if (matcher.find()) {
                            Log.d(TAG, "new vasItem: " + itemTextVas);
                            String title = matcher.group(1);
                            String desc = matcher.group(2);

                            if (desc.contains(")")) desc = desc.substring(0, desc.lastIndexOf(")"));
                            vasItems.add(new VASItem(title, desc, "", ""));
                        }
                    }
                }


                String[] textCancels = textCancel.split(",");
                for (String itemTextCancel : textCancels) {
                    Pattern pattern1 = Pattern.compile("(.+)\\ssoan\\s*(.+)\\sgui\\s(\\d+)");
                    Matcher matcher1 = pattern1.matcher(itemTextCancel.trim());

                    if (matcher1.find()) {
                        String title = matcher1.group(1);
                        String formatCancel = matcher1.group(2);
                        String address = matcher1.group(3);

                        for (int i = 0; i < vasItems.size(); i++) {
                            if (vasItems.get(i).getTitle().equals(title)) {
                                vasItems.get(i).setAddress(address);
                                vasItems.get(i).setFormatCancel(formatCancel);
                                break;
                            }
                        }
                    }
                }
            }

            showResult(context);
        }

        updateTime();
    }

    public void showResult(Context context) {

        sharedPreferences.edit().putBoolean(Constant.sharePreference_key_processing, false).commit();
        resultVas = new ResultVas(vasItems);
        ResultNotification notification = new ResultNotification(context, resultVas);
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.notify(0, notification.build());

        Intent intent = new Intent(context, VASResultActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ResultNotification.VAS_RESULT, resultVas);
        context.startActivity(intent);
    }

    public void updateTime() {
        long currentTime = System.currentTimeMillis();
        sharedPreferences.edit().putLong(Constant.sharePreference_key_time_received, currentTime).commit();
    }

}
