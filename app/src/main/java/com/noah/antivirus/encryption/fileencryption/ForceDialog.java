package com.noah.antivirus.encryption.fileencryption;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.noah.antivirus.R;

/**
 * Created by Hungpq on 10/20/16.
 */
public class ForceDialog extends Dialog {
    private Activity activity;
    private Context context;
    private String message = "";
    private int widthScreen;

    public ForceDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        this.context = activity.getApplicationContext();
    }

    public ForceDialog(Activity activity, String message) {
        super(activity);
        this.activity = activity;
        this.message = message;
        this.context = activity.getApplicationContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_force_finish);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        widthScreen = displayMetrics.widthPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = widthScreen / 5 * 4;
        getWindow().setAttributes(lp);


        /**
         * find and set size to process
         */
        ViewGroup.LayoutParams paramsImgGuide = findViewById(R.id.img_guide).getLayoutParams();
        paramsImgGuide.height = (int) (widthScreen * 4 / 5 * 3 / 4 * 0.6);
        paramsImgGuide.width = widthScreen * 4 / 5 * 5 / 6;
        findViewById(R.id.img_guide).setLayoutParams(paramsImgGuide);
        findViewById(R.id.img_guide).setVisibility(View.VISIBLE);

        /**
         * send intent with action open document tree to get the root tree of sd card
         */
        View.OnClickListener dialogOnclick = new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                try {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    activity.startActivityForResult(intent, 4);

                    //dismiss the dialog when click ok
                    ForceDialog.this.dismiss();

                } catch (ActivityNotFoundException e) {

                    SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.sharePreference_key_setting, Context.MODE_PRIVATE);
                    sharedPreferences.edit().putBoolean(Constant.sharePreference_cant_pick_uri_root_sdcard, true).commit();
                    ForceDialog.this.dismiss();
                }

            }
        };

        ((Button) findViewById(R.id.btn_ok)).setOnClickListener(dialogOnclick);


        if (!message.equals("")) {
            ((TextView) findViewById(R.id.txt_dialog)).setText(message);
        }
    }
}
