package com.noah.antivirus.whypay.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Event.AgreeSendSmsEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Hungpq on 4/14/17.
 */

public class DialogAlertSendSms extends Dialog {
    private Activity activity;

    public DialogAlertSendSms(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_alert_sms);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = width / 4 * 3;
        getWindow().setAttributes(lp);

        findViewById(R.id.btn_destroy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                activity.finish();
            }
        });

        findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                activity.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).edit().putBoolean(Constant.sharePreference_key_agree_send_sms, true).commit();
                EventBus.getDefault().post(new AgreeSendSmsEvent());
            }
        });

        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
    }

}
