package com.noah.antivirus.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

import butterknife.BindView;

public class AppLockSettingsActivity extends BaseToolbarActivity {
    @BindView(R.id.tv_relock_timeout_deription)
    TextView tv_relock_timeout_deription;

    @BindView(R.id.checkbox_applocker_service)
    CheckBox checkbox_applocker_service;

    @BindView(R.id.item_edit_password)
    View item_edit_password;

    @BindView(R.id.checkbox_relock_policy)
    CheckBox checkbox_relock_policy;

    @BindView(R.id.item_relock_timeout)
    View item_relock_timeout;

    @BindView(R.id.item_selfie)
    View item_selfie;

    @BindView(R.id.checkbox_selfie)
    CheckBox checkbox_selfie;

    @BindView(R.id.checkbox_vibrate)
    CheckBox checkbox_vibrate;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_settings;
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();
            init();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.settings);
        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    private void init() {
        checkbox_applocker_service.setChecked(AppLockPreferencesManager.getService(this));
        checkbox_applocker_service.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppLockPreferencesManager.setService(AppLockSettingsActivity.this, isChecked);
                if (isChecked) {
                    monitorShieldService.startLockAppTask();
                } else {
                    monitorShieldService.stopLockAppTask();
                }
            }
        });

        item_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockSettingsActivity.this, AppLockEditPasswordActivity.class));
            }
        });

        checkbox_relock_policy.setChecked(AppLockPreferencesManager.getRelockPolicy(this));
        checkbox_relock_policy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppLockPreferencesManager.setRelockPolicy(AppLockSettingsActivity.this, isChecked);
                if (!isChecked) {
                    AppLockPreferencesManager.setUnlocked(AppLockSettingsActivity.this, false);
                }
            }
        });

        item_selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockSettingsActivity.this, AppLockImagesActivity.class));
            }
        });

        item_relock_timeout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(AppLockSettingsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_single_choice_items);
                WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
                params.width = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setAttributes(params);

                RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_group);
                RadioButton rd_1 = (RadioButton) dialog.findViewById(R.id.rd_1);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_1);
                RadioButton rd_2 = (RadioButton) dialog.findViewById(R.id.rd_2);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_2);
                RadioButton rd_3 = (RadioButton) dialog.findViewById(R.id.rd_3);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_3);
                RadioButton rd_4 = (RadioButton) dialog.findViewById(R.id.rd_4);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_4);

                int lockmode = AppLockPreferencesManager.getRelockTimeOut(AppLockSettingsActivity.this);
                tv_relock_timeout_deription.setText(getLockOptionMode(lockmode));
                switch (lockmode) {
                    case 1:
                        radioGroup.check(R.id.rd_1);
                        break;
                    case 2:
                        radioGroup.check(R.id.rd_2);
                        break;
                    case 3:
                        radioGroup.check(R.id.rd_3);
                        break;
                    case 4:
                        radioGroup.check(R.id.rd_4);
                        break;
                }

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        int values = 1;
                        switch (checkedId) {
                            case R.id.rd_1:
                                values = 1;
                                break;
                            case R.id.rd_2:
                                values = 2;
                                break;
                            case R.id.rd_3:
                                values = 3;
                                break;
                            case R.id.rd_4:
                                values = 4;
                                break;
                        }
                        AppLockPreferencesManager.setRelockTimeOut(AppLockSettingsActivity.this, values);
                        tv_relock_timeout_deription.setText(getLockOptionMode(values));
                    }
                });

                dialog.show();
            }
        });

        checkbox_selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkbox_selfie.isChecked()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        boolean permissionCamera = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                        boolean permissionRead = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                        boolean permissionWrite = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                        if (permissionCamera && permissionRead && permissionWrite) {
                            AppLockPreferencesManager.setSelfie(AppLockSettingsActivity.this, true);
                        } else {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1122);
                        }
                    } else {
                        AppLockPreferencesManager.setSelfie(AppLockSettingsActivity.this, true);
                    }
                } else {
                    AppLockPreferencesManager.setSelfie(AppLockSettingsActivity.this, false);
                }
            }
        });

        checkbox_vibrate.setChecked(AppLockPreferencesManager.getVibrate(this));
        checkbox_vibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppLockPreferencesManager.setVibrate(AppLockSettingsActivity.this, isChecked);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1122:
                boolean permissionCamera = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                boolean permissionRead = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                boolean permissionWrite = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionCamera && permissionRead && permissionWrite) {
                    AppLockPreferencesManager.setSelfie(this, true);
                } else {
                    checkbox_selfie.setChecked(false );
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    private String getLockOptionMode(int mode) {
        switch (mode) {
            case 1:
                return getResources().getString(R.string.after_screen_is_locked);
            case 2:
                return getResources().getString(R.string.after_3_minutes);
            case 3:
                return getResources().getString(R.string.after_5_minutes);
            case 4:
                return getResources().getString(R.string.lock_when_you_launch_an_app);
            default:
                return null;
        }
    }
}
