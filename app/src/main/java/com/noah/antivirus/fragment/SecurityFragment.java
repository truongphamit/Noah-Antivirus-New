package com.noah.antivirus.fragment;

import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.MainActivity;
import com.noah.antivirus.animation.BezierTranslateAnimation;
import com.noah.antivirus.applock.Utils;
import com.noah.antivirus.iface.IOnActionFinished;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppData;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.noahprogressview.NoahProgressView;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecurityFragment extends Fragment implements MonitorShieldService.IClientInterface {

    @BindView(R.id.progressView)
    NoahProgressView progressView;

    @BindView(R.id.tv_count_threat)
    TextView tv_count_threat;

    @BindView(R.id.tv_count_privacy)
    TextView tv_count_privacy;

    @BindView(R.id.tv_count_optimal)
    TextView tv_count_optimal;

    @BindView(R.id.img_threat)
    ImageView img_threat;

    @BindView(R.id.img_privacy)
    ImageView img_privacy;

    @BindView(R.id.img_optimal)
    ImageView img_optimal;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @OnClick(R.id.progressView)
    public void onScan(View view) {
        MonitorShieldService monitorShieldService = ((MainActivity) getActivity()).getMonitorShieldService();
        if (monitorShieldService != null) {
            monitorShieldService.registerClient(this);
            monitorShieldService.scanFileSystem();
        }
    }

    public static SecurityFragment newInstance() {

        Bundle args = new Bundle();

        SecurityFragment fragment = new SecurityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public SecurityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_security, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onMonitorFoundMenace(IProblem menace) {

    }

    @Override
    public void onScanResult(List<PackageInfo> allPackages, Set<IProblem> menacesFound, List<Application> runningApps, List<AppLock> privacy) {
        AppData appData = AppData.getInstance(getActivity());
        appData.setFirstScanDone(true);
        appData.serialize(getActivity());

        startAnimationScanning(allPackages, menacesFound, runningApps, privacy);
    }


    private void init() {

    }

    private void startAnimationScanning(List<PackageInfo> allPackages, Set<IProblem> menacesFound, List<Application> runningApps, List<AppLock> privacy) {
        ScanningTask scanningTask = new ScanningTask(menacesFound, runningApps, privacy);
        scanningTask.set_asyncTaskCallBack(new IOnActionFinished() {
            @Override
            public void onFinished() {
                AppData appData = AppData.getInstance(getActivity());
                appData.setLastScanDate(new DateTime());
                appData.serialize(getActivity());
            }
        });

        scanningTask.execute();
    }

    private class ScanningTask extends AsyncTask<Void, Integer, Void> {
        private IOnActionFinished _asyncTaskCallBack;

        public void set_asyncTaskCallBack(IOnActionFinished _asyncTaskCallBack) {
            this._asyncTaskCallBack = _asyncTaskCallBack;
        }

        private Set<IProblem> menacesFound;
        private List<Application> runningApps;
        private List<AppLock> privacy;

        private boolean _isPaused = false;

        public void pause() {
            _isPaused = true;
        }

        public void resume() {
            _isPaused = false;
        }

        public ScanningTask(Set<IProblem> menacesFound, List<Application> runningApps, List<AppLock> privacy) {
            this.menacesFound = menacesFound;
            this.runningApps = runningApps;
            this.privacy = privacy;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.setPercent(0);
            progressView.setClickable(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int total = 0;
            int currentTotal = 0;
            int currentIndex = 0;
            while (currentIndex < menacesFound.size()) {
                if (!_isPaused) {
                    ++total;
                    ++currentTotal;
                    publishProgress(0, currentTotal, total);
                    ++currentIndex;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            currentTotal = 0;
            currentIndex = 0;
            while (currentIndex < privacy.size()) {
                if (!_isPaused) {
                    ++total;
                    ++currentTotal;
                    publishProgress(1, currentTotal, total);
                    ++currentIndex;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            currentTotal = 0;
            currentIndex = 0;
            while (currentIndex < runningApps.size()) {
                if (!_isPaused) {
                    ++total;
                    currentTotal += runningApps.get(currentIndex).getSize();
                    publishProgress(2, currentTotal, total);
                    ++currentIndex;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int progress = values[2] * 100 / (menacesFound.size() + runningApps.size() + privacy.size());
            progressView.setPercent(progress);
            switch (values[0]) {
                case 0:
                    tv_title.setText(R.string.scanning_for_threat);
                    tv_count_threat.setText(String.valueOf(values[1]));
                    if (values[1] == menacesFound.size()) {
                        tv_count_threat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        setupAnimation(tv_count_threat, img_threat);
                    }
                    break;
                case 1:
                    tv_title.setText(R.string.scanning_for_privacy);
                    tv_count_privacy.setText(String.valueOf(values[1]));
                    if (values[1] == privacy.size()) {
                        tv_count_privacy.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        setupAnimation(tv_count_privacy, img_privacy);
                    }
                    break;
                case 2:
                    tv_title.setText(R.string.freeable_memory);
                    tv_count_optimal.setText(com.noah.antivirus.util.Utils.convertFileSizeToString(values[1] * 1024));
                    if ((values[2] - menacesFound.size() - privacy.size()) == runningApps.size()) {
                        tv_count_optimal.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        setupAnimation(tv_count_optimal, img_optimal);
                    }
                    break;
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressView.setClickable(true);
            if (_asyncTaskCallBack != null)
                _asyncTaskCallBack.onFinished();
        }
    }

    private void setupAnimation(final View start, final View end) {
        start.post(new Runnable() {
            @Override
            public void run() {
                int[] startLocations = new int[2];
                start.getLocationOnScreen(startLocations);
                int startX = startLocations[0] + start.getWidth() / 2;
                int startY = startLocations[1] + start.getHeight() / 2;

                int[] endLocations = new int[2];
                end.getLocationOnScreen(endLocations);
                int endX = endLocations[0] + end.getWidth() / 2;
                int endY = endLocations[1] + end.getHeight() / 2 - 10;

                BezierTranslateAnimation bezierTranslateAnimation = new BezierTranslateAnimation(0, endX - startX, 0, endY - startY, endX - startX, 0);
                bezierTranslateAnimation.setFillAfter(true);
                bezierTranslateAnimation.setDuration(600);
                bezierTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        end.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                start.startAnimation(bezierTranslateAnimation);
            }
        });
    }
}
