package com.noah.antivirus.encryption.fileencryption;

/**
 * Created by Hungpq on 5/18/17.
 */

public class Constant {
    public static String FOLDER_ENCRYPT = "Noah Encryption";
    public static String FOLDER_DECRYPT = "Noah Decryption";
    public static String FILE_ENCRYPT_EXTENSION = "wannacry";
    public static String FILE_DECRYPT_EXTENSION = "jpg";

    public static String sharePreference_key_setting = "encryptSetting";

    public static String getSharePreference_uri_root_sdcard = "URI";
    public static String sharePreference_cant_pick_uri_root_sdcard = "cant_pick_uri_root_sdcard";


}
