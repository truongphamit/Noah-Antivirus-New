package com.noah.antivirus.encryption.fileencryption;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import in.myinnos.awesomeimagepicker.models.Image;

/**
 * Created by Hungpq on 5/19/17.
 *
 * encode image using base64 with imei hash
 */

public class EncodeTask extends AsyncTask<ArrayList<Image>, Integer, Boolean> {
    public static String TAG = "EncodeTask";
    private Context context;

    public EncodeTask(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(ArrayList<Image>... arrayLists) {
        for (Image image : arrayLists[0]) {
            encodeFile(image.path, image.name);
        }
        return true;
    }

    /**
     * encode a file using base64
     */
    public void encodeFile(String filePath, String fileEncryptionName) {

        Bitmap bm = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Log.d(TAG, bm.toString());

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] imageBytes = baos.toByteArray();
        String input = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        writeToFile(input, fileEncryptionName);

        File oldImg = new File(filePath);
        Utils.deleteFile(oldImg, context);

    }

    /**
     * write encrypted file
     *
     * @param data:    base64 encrypt of bitmap image
     * @param fileName
     */
    private void writeToFile(String data, String fileName) {
        String environmentDirectory = Environment.getExternalStorageDirectory().toString();
        File myAppFolder = new File(environmentDirectory, Constant.FOLDER_ENCRYPT);
        myAppFolder.mkdir();
        String fileRealName = Utils.getFileRealName(fileName);
        Log.d(TAG, fileName + " " + fileRealName);

        fileName = fileRealName + "." + Constant.FILE_ENCRYPT_EXTENSION;

        File file = new File(myAppFolder, fileName);
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            stream.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
