package com.noah.antivirus.applock;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

/**
 * Created by truon on 8/23/17.
 */

public class AppLockPreferencesManager {
    private static final String PREFERENCES_APPLOCK_PASSWORD = "password";
    private static final String PREFERENCES_APPLOCK_QUESTION = "question";
    private static final String PREFERENCES_APPLOCK_ANSWER = "answer";
    private static final String PREFERENCES_APPLOCK_SERVICE = "applocker_service";
    private static final String PREFERENCES_APPLOCK_RELOCK_POLICY = "relock_policy";
    private static final String PREFERENCES_APPLOCK_RELOCK_TIMEOUT = "timeout";
    public static final String PREFERENCES_APPLOCK_SELFIE = "selfie";
    public static final String PREFERENCES_APPLOCK_VIBRATE = "vibrate";
    public static final String PREFERENCES_APPLOCK_THIEVES = "thieves";
    public static final String PREFERENCES_APPLOCK_APP_THIEVES = "app_thieves";
    public static final String PREFERENCES_APPLOCK_UNLOCKED = "unlocked";

    public static SharedPreferences getPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setPassword(Context context, String pass) {
        getPreference(context).edit().putString(PREFERENCES_APPLOCK_PASSWORD, pass).apply();
    }

    public static String getPassword(Context context) {
        return getPreference(context).getString(PREFERENCES_APPLOCK_PASSWORD, null);
    }

    public static void setQuestion(Context context, String question) {
        getPreference(context).edit().putString(PREFERENCES_APPLOCK_QUESTION, question).apply();
    }

    public static String getQuestion(Context context) {
        return getPreference(context).getString(PREFERENCES_APPLOCK_QUESTION, "");
    }

    public static void setAnswer(Context context, String answer) {
        getPreference(context).edit().putString(PREFERENCES_APPLOCK_ANSWER, answer).apply();
    }

    public static String getAnswer(Context context) {
        return getPreference(context).getString(PREFERENCES_APPLOCK_ANSWER, "");
    }

    public static void setService(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_SERVICE, isOpen).apply();
    }

    public static boolean getService(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_SERVICE, true);
    }

    public static void setRelockPolicy(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_RELOCK_POLICY, isOpen).apply();
    }

    public static boolean getRelockPolicy(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_RELOCK_POLICY, false);
    }

    public static void setRelockTimeOut(Context context, int timeout) {
        getPreference(context).edit().putInt(PREFERENCES_APPLOCK_RELOCK_TIMEOUT, timeout).apply();
    }

    public static int getRelockTimeOut(Context context) {
        return getPreference(context).getInt(PREFERENCES_APPLOCK_RELOCK_TIMEOUT, 1);
    }

    public static void setSelfie(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_SELFIE, isOpen).apply();
    }

    public static boolean getSelfie(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_SELFIE, false);
    }

    public static void setVibrate(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_VIBRATE, isOpen).apply();
    }

    public static boolean getVibrate(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_VIBRATE, false);
    }

    public static void setThieves(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_THIEVES, isOpen).apply();
    }

    public static boolean getThieves(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_THIEVES, false);
    }

    public static void setAppThieves(Context context, long timeout) {
        getPreference(context).edit().putLong(PREFERENCES_APPLOCK_APP_THIEVES, timeout).apply();
    }

    public static long getAppThieves(Context context) {
        return getPreference(context).getLong(PREFERENCES_APPLOCK_APP_THIEVES, -1);
    }

    public static void setUnlocked(Context context, boolean isOpen) {
        getPreference(context).edit().putBoolean(PREFERENCES_APPLOCK_UNLOCKED, isOpen).apply();
    }

    public static boolean getUnlocked(Context context) {
        return getPreference(context).getBoolean(PREFERENCES_APPLOCK_UNLOCKED, false);
    }
}
