package com.noah.antivirus.whypay.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Hungpq on 4/11/17.
 */

public class ResultVas implements Serializable {

    private ArrayList<VASItem> vasItems;

    public ResultVas(ArrayList<VASItem> vasItems) {
        this.vasItems = vasItems;
    }

    public ArrayList<VASItem> getVasItems() {
        return vasItems;
    }

    public void setVasItems(ArrayList<VASItem> vasItems) {
        this.vasItems = vasItems;
    }
}
