package com.noah.antivirus.browser;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.browser.activities.BrowserSettingsActivity;
import com.noah.antivirus.browser.adpaters.TabRecentsAdapter;
import com.noah.antivirus.browser.customview.RecentsList;
import com.noah.antivirus.browser.fragments.TabWebFragment;
import com.noah.antivirus.browser.models.BookmarkManager;
import com.noah.antivirus.browser.models.WeatherItem;
import com.noah.antivirus.browser.networks.WeatherService;
import com.noah.antivirus.browser.tools.AdBlocker;
import com.noah.antivirus.browser.tools.BrowserSharePreference;
import com.noah.antivirus.browser.webview.AdvancedWebView;
import com.noah.antivirus.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainBrowserActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.recents)
    RecentsList recents;

    @BindView(R.id.img_add)
    ImageView img_add;

    @BindView(R.id.ll_recents)
    View ll_recents;

    @BindView(R.id.img_header)
    View img_header;

    @BindView(R.id.tv_count)
    TextView tv_count;

    private List<TabWebFragment> fragments;
    private TabRecentsAdapter adapter;
    private BookmarkManager bookmarkManager;
    private int currentTab;

    public List<TabWebFragment> getFragments() {
        return fragments;
    }

    public int getCurrentTab() {
        return currentTab;
    }

    public BookmarkManager getBookmarkManager() {
        return bookmarkManager;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Night mode theme
        if (BrowserSharePreference.getNightMode(this)) {
            setTheme(R.style.Theme_Dark);
        } else {
            setTheme(R.style.Theme_Light);
        }

        AdBlocker.init(this);
        setContentView(R.layout.activity_main_browser);
        ButterKnife.bind(this);
        init();
        getSupportFragmentManager().beginTransaction().replace(R.id.container_webview, fragments.get(currentTab)).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bookmarkManager = new BookmarkManager(this);
    }

    private void init() {
        fragments = new ArrayList<>();
        fragments.add(TabWebFragment.newInstance());
        adapter = new TabRecentsAdapter(this, fragments);
        recents.setAdapter(adapter);
        recents.setOnItemClickListener(new RecentsList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                currentTab = position;
                showTab(fragments.get(currentTab));
            }
        });
        tv_count.setText(String.valueOf(getNumberOfTabs()));

        adapter.setOnTabRemoveListener(new TabRecentsAdapter.OnTabRemoveListener() {
            @Override
            public void onRemove(int position) {
                tv_count.setText(String.valueOf(getNumberOfTabs()));
                if (fragments.isEmpty()) {
                    currentTab = 0;
                } else {
                    currentTab = fragments.size() - 1;
                }
            }
        });

        img_add.setOnClickListener(this);
        img_header.setOnClickListener(this);
    }

    public void showAllTab() {
        tv_count.setText(String.valueOf(getNumberOfTabs()));
        recents.updateLayout();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        ft.remove(manager.findFragmentById(R.id.container_webview));
        ft.commit();

        ll_recents.setVisibility(View.VISIBLE);
    }

    public int getNumberOfTabs() {
        return fragments.size();
    }

    public void showTab(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

        ft.replace(R.id.container_webview, fragment);
        ft.commit();
        ll_recents.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_add:
                fragments.add(TabWebFragment.newInstance());
                currentTab = fragments.size() - 1;
                showTab(fragments.get(currentTab));
                break;
            case R.id.img_header:
                showFilterPopup(v);
                break;
        }
    }

    // Display anchored popup menu based on view selected
    private void showFilterPopup(View v) {
        final PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.getMenuInflater().inflate(R.menu.main_browser, popup.getMenu());
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_close_all_tabs:
                        closeAllTabs();
                        return true;
                    case R.id.menu_settings:
                        popup.dismiss();
                        startActivity(new Intent(MainBrowserActivity.this, BrowserSettingsActivity.class));
                        return true;
                    case R.id.menu_exit:
                        finish();
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    private void closeAllTabs() {
        tv_count.setText(String.valueOf(0));
        fragments.clear();
        recents.updateLayout();
    }

    @Override
    public void onBackPressed() {
        if (ll_recents.getVisibility() == View.VISIBLE) {
            if (fragments.isEmpty()) {
                fragments.add(TabWebFragment.newInstance());
            }
            showTab(fragments.get(currentTab));
            return;
        }

        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            super.onBackPressed();
        } else {
            TabWebFragment tabWebFragment = (TabWebFragment) getSupportFragmentManager().findFragmentById(R.id.container_webview);
            if (tabWebFragment != null) {
                AdvancedWebView webView = tabWebFragment.getWebView();
                if (webView.canGoBack()) {
                    webView.goBack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    public void openLinkNewTab(String url) {
        TabWebFragment tabWebFragment = TabWebFragment.newInstance();
        tabWebFragment.setUrl(url);
        fragments.add(tabWebFragment);
        showTab(tabWebFragment);
    }
}
