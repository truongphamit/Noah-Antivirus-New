package com.noah.antivirus.iface;

/**
 * Created by hexdump on 20/01/16.
 */
public interface IOnActionFinished
{
    public void onFinished();
}
