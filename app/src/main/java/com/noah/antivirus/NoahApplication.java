package com.noah.antivirus;

import android.app.Application;

import com.blankj.utilcode.util.Utils;
import com.noah.antivirus.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by truongpq on 23/03/2017.
 */

public class NoahApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/arial.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
