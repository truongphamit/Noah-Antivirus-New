package com.noah.antivirus.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.PhoneBoostActivity;
import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.browser.MainBrowserActivity;
import com.noah.antivirus.browser.tools.StaticTools;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.activities.AppLockScreenActivity;
import com.noah.antivirus.activities.IgnoredListActivity;
import com.noah.antivirus.activities.PhoneInfoActivity;
import com.noah.antivirus.whypay.Activity.VasCheckerActivity;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.wifisecurity.WifiSecurityActivity;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by HuyLV-CT on 6/21/2016.
 */
public abstract class BaseNavigationDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer_layout;

    private boolean doubleBackToExitPressedOnce = false;

    public static final String BROADCAST_ALLOW_ACCESSIBILITY = "BROADCAST_ALLOW_ACCESSIBILITY";
    public static final String SHOULD_CREATE_PASS = "SHOULD_CREATE_PASS";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        /**
         * use the custom font
         */
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/UTM Avo.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();

        initMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(allowAccessibilityService, new IntentFilter(BROADCAST_ALLOW_ACCESSIBILITY));
    }

    private void initMenu() {
        final View phoneInfo = drawer_layout.findViewById(R.id.menu_phone_info);
        phoneInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, PhoneInfoActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        final View showIgnoredList = drawer_layout.findViewById(R.id.menu_show_ignored_list);
        showIgnoredList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, IgnoredListActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        final SharedPreferences settings = getSharedPreferences("Settings", 0);
        Switch toggleAutoScan = (Switch) drawer_layout.findViewById(R.id.toggle_auto_scan);
        toggleAutoScan.setChecked(settings.getBoolean("auto_scan", true));
        toggleAutoScan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = settings.edit();
                if (isChecked) {
                    editor.putBoolean("auto_scan", true);
                    editor.apply();
                } else {
                    editor.putBoolean("auto_scan", false);
                    editor.apply();
                }
            }
        });

        View appLock = findViewById(R.id.menu_app_lock);
        appLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.noah.antivirus.applock.Utils.openAppLock(BaseNavigationDrawerActivity.this);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View rateUs = drawer_layout.findViewById(R.id.menu_rate_us);
        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.rate(BaseNavigationDrawerActivity.this);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View moreApp = drawer_layout.findViewById(R.id.menu_more_app);
        moreApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.moreApp(BaseNavigationDrawerActivity.this, getString(R.string.store_dev_id));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View feedback = drawer_layout.findViewById(R.id.menu_feedback);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.feedback(BaseNavigationDrawerActivity.this);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View browser = drawer_layout.findViewById(R.id.menu_browser);
        browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, MainBrowserActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        drawer_layout.findViewById(R.id.menu_phone_boost).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, PhoneBoostActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        if (com.noah.antivirus.whypay.Utils.Utils.checkSimType(getApplicationContext()) == Constant.telecom_type_not_detect) {
            drawer_layout.findViewById(R.id.menu_vas_checker).setVisibility(View.GONE);
        } else {
            drawer_layout.findViewById(R.id.menu_vas_checker).setVisibility(View.VISIBLE);
            drawer_layout.findViewById(R.id.menu_vas_checker).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(BaseNavigationDrawerActivity.this, VasCheckerActivity.class));
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
            });
        }

        drawer_layout.findViewById(R.id.menu_wifi_security).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, WifiSecurityActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.click_back_again_to_exit, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(allowAccessibilityService);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * event when user allow accessibility service and auto boost device
     */
    public BroadcastReceiver allowAccessibilityService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getBooleanExtra(SHOULD_CREATE_PASS, false)) {
                    if (AppLockPreferencesManager.getPassword(context) == null) {
                        startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockCreatePasswordActivity.class));
                    } else {
                        startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockScreenActivity.class));
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };
}
