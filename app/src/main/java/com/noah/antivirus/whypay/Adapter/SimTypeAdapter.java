package com.noah.antivirus.whypay.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Model.SimType;

import java.util.List;

/**
 * Created by Hungpq on 4/10/17.
 */

public class SimTypeAdapter extends RecyclerView.Adapter<SimTypeAdapter.ViewHolder> {

    private List<SimType> simTypes;

    public SimTypeAdapter(List<SimType> simTypes) {
        this.simTypes = simTypes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sim_type_picker, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(simTypes.get(position));
    }

    @Override
    public int getItemCount() {
        return simTypes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgIsChosen;
        private TextView txtSimType;

        public ViewHolder(View itemView) {
            super(itemView);

            imgIsChosen = (ImageView) itemView.findViewById(R.id.img_sim_chosen);
            txtSimType = (TextView) itemView.findViewById(R.id.txt_sim_name);
        }

        public void setData(SimType simType) {
            if (simType.isChosen()) {
                imgIsChosen.setVisibility(View.VISIBLE);
            } else {
                imgIsChosen.setVisibility(View.GONE);
            }

            txtSimType.setText(simType.getSimTypeName());
        }
    }

}
