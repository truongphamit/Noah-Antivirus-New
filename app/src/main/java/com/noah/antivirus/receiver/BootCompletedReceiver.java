package com.noah.antivirus.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.Utils;

/**
 * Created by hexdump on 15/01/16.
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Utils.isServiceRunning(context, MonitorShieldService.class)) {
            Intent myIntent = new Intent(context, MonitorShieldService.class);
            context.startService(myIntent);
        }
    }
}