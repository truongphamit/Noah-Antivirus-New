package com.noah.antivirus.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.noah.antivirus.adapter.QuestionSpinnerAdapter;
import com.noah.antivirus.applock.AppLockPreferencesManager;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.R;

import java.util.List;

import butterknife.BindView;

public class AppLockCreatePasswordActivity extends BaseToolbarActivity {
    @BindView(R.id.la_password)
    View la_password;

    @BindView(R.id.la_question)
    View la_question;

    @BindView(R.id.lock_view)
    PatternLockView lock_view;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.spinner_question)
    Spinner spinner_question;

    @BindView(R.id.done)
    TextView done;

    @BindView(R.id.edt_answer)
    EditText edt_answer;

    private String password;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_create_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        tv_title.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        QuestionSpinnerAdapter spinnerAdapter = new QuestionSpinnerAdapter(this, getResources().getStringArray(R.array.question_arrays));
        spinner_question.setAdapter(spinnerAdapter);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDone();
            }
        });

        edt_answer.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    actionDone();
                }
                return false;
            }
        });

        if (AppLockPreferencesManager.getVibrate(this)) {
            lock_view.setTactileFeedbackEnabled(true);
        } else {
            lock_view.setTactileFeedbackEnabled(false);
        }

        lock_view.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                if (password == null) {
                    password = PatternLockUtils.patternToString(lock_view, pattern);
                    lock_view.clearPattern();
                    tv_title.setText(R.string.enter_password_again);
                } else {
                    if (!password.equals(PatternLockUtils.patternToString(lock_view, pattern))) {
                        lock_view.setViewMode(PatternLockView.PatternViewMode.WRONG);
                    } else {
                        lock_view.setViewMode(PatternLockView.PatternViewMode.CORRECT);
                        la_password.setVisibility(View.GONE);
                        la_question.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCleared() {

            }
        });
    }

    private void actionDone() {
        if (edt_answer.getText().toString().length() == 0) {
            Snackbar snackbar = Snackbar.make(la_question, R.string.answer_is_not_empty, Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            AppLockPreferencesManager.setPassword(this, password);
            AppLockPreferencesManager.setQuestion(this, spinner_question.getSelectedItem().toString());
            AppLockPreferencesManager.setAnswer(this, edt_answer.getText().toString());

            startActivity(new Intent(AppLockCreatePasswordActivity.this, AppLockHomeActivity.class));
            finish();
        }
    }
}
