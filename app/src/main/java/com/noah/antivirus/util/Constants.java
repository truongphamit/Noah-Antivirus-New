package com.noah.antivirus.util;

/**
 * Created by HuyLV-CT on 7/4/2016.
 */
public interface Constants {
    String INTENT_NUM_OF_PACKAGES = "INTENT_NUM_OF_PACKAGES";
    long ERROR = 0;


    //Tools ID
    int TOOLS_WIFI = 0;
    int TOOLS_BROWSER = 1;
    int TOOLS_BOOST = 2;

    //Privacy ID
    int PRIVACY_APPLOCK = 0;
    int PRIVACY_NOTE = 1;
    int PRIVACY_IMAGE = 2;
    int PRIVACY_VIDEO = 3;
    int PRIVACY_CALL = 4;
    int PRIVACY_ALERT = 5;
    int PRIVACY_VAS = 6;
}
