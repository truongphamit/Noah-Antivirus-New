package com.noah.antivirus.model;

/**
 * Created by truon on 8/14/17.
 */

public class Tool {
    private int id;
    private int resIcon;
    private String title;
    private int resHeader;
    private String action;

    public Tool(int id, int resIcon, String title, int resHeader, String action) {
        this.id = id;
        this.resIcon = resIcon;
        this.title = title;
        this.resHeader = resHeader;
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResHeader() {
        return resHeader;
    }

    public void setResHeader(int resHeader) {
        this.resHeader = resHeader;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
