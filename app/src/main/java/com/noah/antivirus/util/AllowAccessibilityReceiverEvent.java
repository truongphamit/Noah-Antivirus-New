package com.noah.antivirus.util;

/**
 * Created by Hungpq on 3/14/17.
 */

public class AllowAccessibilityReceiverEvent {

    private boolean isRegister;

    public AllowAccessibilityReceiverEvent(boolean isRegister) {
        this.isRegister = isRegister;
    }

    public boolean isRegister() {
        return isRegister;
    }

    public void setRegister(boolean register) {
        isRegister = register;
    }
}
