package com.noah.antivirus.whypay.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.noah.antivirus.R;

/**
 * Created by Hungpq on 4/18/17.
 */

public class DialogContentCancel extends Dialog {
    private Activity activity;
    String msgFrom, msgBody;

    public DialogContentCancel(Activity activity, String msgBody, String msgFrom) {
        super(activity);
        this.activity = activity;
        this.msgBody = msgBody;
        this.msgFrom = msgFrom;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_content_sms);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = width / 4 * 3;
        getWindow().setAttributes(lp);

        ((TextView) findViewById(R.id.txt_address)).setText(msgFrom);
        ((TextView) findViewById(R.id.txt_msg_body)).setText(msgBody);

        findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
